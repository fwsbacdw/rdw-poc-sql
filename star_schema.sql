﻿#-----------------------------------------------------------------------------------------------------------
#-- simulate a desired star schema based on the old data model/schema
#-----------------------------------------------------------------------------------------------------------
#

#-----------------------------------------------------------------------------------------------------------
#-- dimension state
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE edware_poc.poc_dim_state (
  id tinyint NOT NULL AUTO_INCREMENT,
  state_code varchar(2),
  PRIMARY KEY (id)
);

#-----------------------------------------------------------------------------------------------------------
#-- dimension district
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE  edware_poc.poc_dim_district (
  id mediumint NOT NULL AUTO_INCREMENT,
  state_id tinyint NOT NULL,
  district_id_old varchar(40) NOT NULL,
  district_name varchar(60) NOT NULL,
  PRIMARY KEY (id)
);

#-----------------------------------------------------------------------------------------------------------
#-- dimension assessment
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE edware_poc.poc_dim_asmt (
asmt_id bigint, 
asmt_grade varchar(2),
asmt_year mediumint, 
asmt_type varchar(32), 
asmt_subject varchar(64),
PRIMARY KEY(asmt_id));

#-----------------------------------------------------------------------------------------------------------
#-- dimension gender
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE edware_poc.poc_dim_gender
(id tinyint NOT NULL, 
  value varchar(10),
  PRIMARY KEY (id));
  
INSERT INTO edware_poc.poc_dim_gender
  (SELECT 0 as id, 'undefined' as value) union
  (SELECT 1 as id, 'female' as value) union
  (SELECT 2 as id, 'male' as value);

#-----------------------------------------------------------------------------------------------------------
#-- dimension ethnicity
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE edware_poc.poc_dim_ethn
(id tinyint NOT NULL, 
  value varchar(20),
  PRIMARY KEY(id))
	(SELECT 0 as id,  'None' as value) union
	(SELECT 1 as id,  'African American' as value) union
	(SELECT 2 as id,  'Asian' as value) union
	(SELECT 3 as id,  'Hispanic' as value) union
	(SELECT 4 as id,  'American Indian' as value) union
	(SELECT 5 as id,  'Pacific Islander' as value) union
	(SELECT 6 as id,  'White' as value) union
	(SELECT 7 as id,  'Two or more races' as value);

#-----------------------------------------------------------------------------------------------------------
#-- dimension institution hierarchy
#---------------------------------------------------------------------------------------------------------

CREATE TABLE edware_poc.poc_dim_school (
  id int NOT NULL AUTO_INCREMENT,
  school_guid varchar(40),
  school_name varchar(60),
  district_id mediumint,
  PRIMARY KEY(id));


#-----------------------------------------------------------------------------------------------------------
#-- dimension student
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE edware_poc.poc_dim_student
(id bigint NOT NULL, 
  gender_id tinyint,
  ethn_id tinyint,
  PRIMARY KEY(id));


#-----------------------------------------------------------------------------------------------------------
#-- fact tables.
#-- NOTE: student attributes(gender and ethnicity) as well as the institution hierarchy are de-normalized
#-- An alternative is to move them into appropriate dimensions
#-----------------------------------------------------------------------------------------------------------
CREATE TABLE  edware_poc.poc_fact_asmt (
  id bigint NOT NULL AUTO_INCREMENT,
  state_id tinyint NOT NULL,
  district_id mediumint NOT NULL,
  school_id int NOT NULL,
  student_id bigint NOT NULL,
  gender_id tinyint NOT NULL,
  ethn_id tinyint DEFAULT NULL,
  asmt_id bigint NOT NULL,
  asmt_score smallint NOT NULL,
  asmt_grade varchar(2) NOT NULL,
  asmt_year tinyint NOT NULL,
  level tinyint NOT NULL,
  level1_value tinyint NOT NULL,
  level2_value tinyint NOT NULL,
  level3_value tinyint NOT NULL,
  level4_value tinyint NOT NULL,
  PRIMARY KEY (id)
);


#----------------------------------------------------------------------------------------------------------
#-- set up foreign keys  -- Should we have any ON UPDATE and ON DELETE reference option? Like CASCADE
#----------------------------------------------------------------------------------------------------------
ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (asmt_id) REFERENCES edware_poc.poc_dim_asmt(asmt_id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (state_id) REFERENCES edware_poc.poc_dim_state(id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (district_id) REFERENCES edware_poc.poc_dim_district(id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (school_id) REFERENCES edware_poc.poc_dim_school(id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (student_id) REFERENCES edware_poc.poc_dim_student(id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (gender_id) REFERENCES edware_poc.poc_dim_gender(id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (ethn_id) REFERENCES edware_poc.poc_dim_ethn(id);

ALTER TABLE edware_poc.poc_fact_asmt
    ADD FOREIGN KEY (ethn_id) REFERENCES edware_poc.poc_dim_ethn(id);


#-----------------------------------------------------------------------------------------------------------
#-- Insert data in to tables
#-----------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------------------------
#-- State insert
#-----------------------------------------------------------------------------------------------------------
INSERT INTO edware_poc.poc_dim_state (
 state_code
)
SELECT DISTINCT state_code from edware.dim_inst_hier;

#-----------------------------------------------------------------------------------------------------------
#-- District insert
#-----------------------------------------------------------------------------------------------------------
INSERT INTO  edware_poc.poc_dim_district (
  state_id,
  district_id_old,
  district_name
)
SELECT DISTINCT ds.id, district_id, district_name
FROM  edware.dim_inst_hier as dh JOIN edware_poc.poc_dim_state as ds ON ds.state_code = dh.state_code;

#-----------------------------------------------------------------------------------------------------------
#-- School insert
#-----------------------------------------------------------------------------------------------------------
INSERT INTO edware_poc.poc_dim_school (
school_guid,
school_name,
district_id
)
SELECT DISTINCT school_id as school_guid, school_name as school_name, dd.id as district_id
FROM edware.dim_inst_hier as dh JOIN edware_poc.poc_dim_district as dd on dh.district_id = dd.district_id_old;

#-----------------------------------------------------------------------------------------------------------
#-- Student insert
#-----------------------------------------------------------------------------------------------------------
INSERT INTO edware_poc.poc_dim_student (
  SELECT DISTINCT fact_asmt_outcome_vw.student_rec_id AS id,
        CASE fact_asmt_outcome_vw.sex
            WHEN 'female' THEN 1
            ELSE 2
        END AS gender_id,
    fact_asmt_outcome_vw.dmg_eth_derived AS ethn_id
  FROM edware.fact_asmt_outcome_vw
);

#-----------------------------------------------------------------------------------------------------------
#-- Assessment insert
#-----------------------------------------------------------------------------------------------------------
INSERT INTO edware_poc.poc_dim_asmt (
  SELECT DISTINCT
    asmt_rec_id as asmt_id,
    asmt_grade,
    asmt_year,
    asmt_type,
    asmt_subject
  FROM edware.fact_asmt_outcome_vw
);


#-----------------------------------------------------------------------------------------------------------
#-- Fact insert
#-----------------------------------------------------------------------------------------------------------
INSERT INTO edware_poc.poc_fact_asmt (
  state_id,
  district_id,
  school_id, 
  student_id,
  gender_id,
  ethn_id,
  asmt_id,
  asmt_score,
  asmt_grade,
  asmt_year,
  level,
  level1_value,
  level2_value,
  level3_value,
  level4_value
)
SELECT 
    1 as state_id,
    dsch.district_id as district_id,
    dsch.id as school_id,
    fact.student_rec_id as student_id,
    case fact.sex when 'female' then 1 else 2 end as gender_id,
    fact.dmg_eth_derived as ethn_id,
    fact.asmt_rec_id as asmt_id,
    fact.asmt_score as asmt_score,
    fact.asmt_grade as asmt_grade,
    fact.asmt_year as asmt_year,
    fact.asmt_perf_lvl as level,
    case fact.asmt_perf_lvl when 1 then 1 else 0 end as level1_value,
    case fact.asmt_perf_lvl when 2 then 1 else 0 end as level2_value,
    case fact.asmt_perf_lvl when 3 then 1 else 0 end as level3_value,
    case fact.asmt_perf_lvl when 4 then 1 else 0 end as level4_value
FROM edware.fact_asmt_outcome_vw as fact 
  JOIN (
    edware_poc.poc_dim_school as dsch
  ) 
  ON (
    fact.school_id = dsch.school_guid
  );



