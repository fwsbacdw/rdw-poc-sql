# README #


### What is this repository for? ###

This repository contains sample SQL queries for aggregate reports POC. 

### How do I get set up? ###

The queries are written for PostgreSQL, and assumed that the old edware DB schema is created (as edware_ca) and loaded with the data. 

To execute the queries, first run *star_schema.sql* to crate views that simulate a star schema for the aggregate reports.


