﻿-----------------------------------------------------------------------------------------------------------
-- Option 2: the output from this option is similar to option 1

---NOTE: not testable on Aurora

-- A problem with this option is that if only individual schools are needed, in order to get a district or
-- state roll ups, all schools must be aggregated
-----------------------------------------------------------------------------------------------------------
WITH school_roll_up AS (
    SELECT
      count(*) AS count,
      gender_id,
      asmt_grade,
      asmt_year,
      school_name,
      a.school_id,
      district_name,
      a.district_id,
      a.state_code
    FROM edware_ca.poc_fact_asmt a
      INNER JOIN edware_ca.poc_dim_school h ON h.id = a.school_id
      JOIN edware_ca.poc_dim_asmt da on a.asmt_id = da.id
    GROUP BY asmt_grade
      , asmt_year
      , school_name
      , a.school_id
      , district_name
      , a.district_id
      , a.state_code
      , gender_id
)
SELECT
  sum(count)
  OVER (
    PARTITION BY school_id
      , asmt_year
      , gender_id
      , asmt_grade
  ) AS school_count,
  sum(count)
  OVER (
    PARTITION BY district_id
      , asmt_year
      , gender_id
      ,asmt_grade
  ) AS district_count,
  sum(count)
  OVER (
    PARTITION BY state_code
      , asmt_year
      , gender_id
      ,asmt_grade
  ) AS state_count,
  *
FROM school_roll_up
ORDER BY asmt_year
  , district_name
  , school_name
  , gender_id
  , asmt_grade