﻿------------------------------------------------------------------------------------------------------------
-- count of students tested (without cohort) with the max score per assessment year/grade/subject
------------------------------------------------------------------------------------------------------------
SELECT
  count(*),
  cast(avg(score) AS INT),
  asmt_grade,
  asmt_year,
  school_name
FROM (
       SELECT
         student_id,
         max(score) AS score,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_name
       FROM edware_ca.poc_fact_asmt fa
         JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
         JOIN edware_ca.poc_dim_school dh ON dh.id = fa.school_id
       WHERE
         asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
         and
         (
           (asmt_year = 2015 and asmt_grade = '3') or
           (asmt_year = 2016 and asmt_grade = '4') or
           (asmt_year = 2017 and asmt_grade = '5')
         )
       GROUP BY
         student_id,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_name
     ) student_max_score
GROUP BY
  asmt_grade
  , asmt_year
  , school_name
ORDER BY school_name,
   asmt_year,
  asmt_grade
