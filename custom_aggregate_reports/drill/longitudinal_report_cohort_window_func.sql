﻿------------------------------------------------------------------------------------------------------------
-- group of students in cohort throughout all selected years
------------------------------------------------------------------------------------------------------------
SELECT
  count(*),
  cast(avg(score) AS INT),
  asmt_grade,
  asmt_year,
  school_id
FROM (
  -- get the highest student score within a school
  SELECT
    student_rec_id,
    max(score) AS score,
    asmt_grade,
    asmt_year,
    asmt_type,
    asmt_subject,
    school_id,
    count(*) OVER (PARTITION BY student_rec_id, school_id) AS year_in_one_inst
  FROM s3.tmp.tinycap
  WHERE
    asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
    AND
    (
      (asmt_year = 2015 AND asmt_grade = '3') OR
      (asmt_year = 2016 AND asmt_grade = '4') OR
      (asmt_year = 2017 AND asmt_grade = '5')
    )
  GROUP BY student_rec_id, asmt_grade, asmt_year, asmt_type, asmt_subject, school_id
) s
-- this needs to match the number of years included, it removes students that are not found in all the years
WHERE year_in_one_inst = 3
GROUP BY
asmt_grade
, asmt_year
, school_id
ORDER BY school_id
, asmt_year
, asmt_grade

-- https://issues.apache.org/jira/browse/DRILL-3549

-- ORDER BY school_id
-- , asmt_year
-- , asmt_grade
EXPLAIN plan for
SELECT
  count(*),
  avg(score),
  asmt_grade,
  asmt_year,
  school_id
FROM (
  -- get the highest student score within a school
  SELECT
    student_rec_id,
    max(EXPR$10) AS score,
    asmt_grade,
    asmt_year,
    asmt_type,
    asmt_subject,
    school_id,
    count(*) OVER (PARTITION BY student_rec_id, school_id) AS year_in_one_inst
  FROM s3.tmp.sampleparquet4
  WHERE
    asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
    AND
    (
      (asmt_year = 2015 AND asmt_grade = '3') OR
      (asmt_year = 2016 AND asmt_grade = '4') OR
      (asmt_year = 2017 AND asmt_grade = '5')
    )
  GROUP BY student_rec_id, asmt_grade, asmt_year, asmt_type, asmt_subject, school_id
) s
-- this needs to match the number of years included, it removes students that are not found in all the years
WHERE year_in_one_inst = 3
GROUP BY
asmt_grade
, asmt_year
, school_id
ORDER BY school_id
, asmt_year
, asmt_grade;
-- 65.889 seconds

------
SELECT
  count(*),
  avg(score),
  asmt_grade,
  asmt_year,
  school_id
FROM (
  -- get the highest student score within a school
  SELECT
    student_rec_id,
    max(EXPR$10) AS score,
    asmt_grade,
    asmt_year,
    asmt_type,
    asmt_subject,
    school_id,
    count(*) OVER (PARTITION BY student_rec_id, school_id) AS year_in_one_inst
  FROM s3.tmp.sampleparquetwithpartition
  WHERE
    asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
    AND
    (
      (asmt_year = 2015 AND asmt_grade = '3') OR
      (asmt_year = 2016 AND asmt_grade = '4') OR
      (asmt_year = 2017 AND asmt_grade = '5')
    )
  GROUP BY student_rec_id, asmt_grade, asmt_year, asmt_type, asmt_subject, school_id
) s
-- this needs to match the number of years included, it removes students that are not found in all the years
WHERE year_in_one_inst = 3
GROUP BY
asmt_grade
, asmt_year
, school_id
ORDER BY school_id
, asmt_year
, asmt_grade;
-- 48.356 sec