﻿---
--- create table s3.tmp.`tinycap` as
--- (select student_rec_id, state_code, district_id, school_id, asmt_type, asmt_year, asmt_subject, asmt_grade, cast(asmt_score as int) as score, asmt_perf_lvl from s3.tiny_test.`fact_asmt_outcome_vw.csv`)

SELECT count(*), avg(score), sex, asmt_grade, asmt_year,  school_id FROM s3.tmp.tinycap WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year,school_id,district_id, sex
--- UNION ALL issue :https://issues.apache.org/jira/browse/DRILL-4958
UNION ALL

SELECT count(*), avg(score), sex, asmt_grade, asmt_year, district_id FROM s3.tmp.tinycap WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year,district_id, sex

UNION ALL

SELECT count(*), avg(score), sex, asmt_grade, asmt_year, state_code FROM s3.tmp.tinycap WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year, state_code , sex

-------------------------------------------

EXPLAIN plan for
SELECT count(*), avg(EXPR$10), sex, asmt_grade, asmt_year,  school_id as name FROM s3.tmp.sampleparquet4 WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year,school_id, sex
union all
SELECT count(*), avg(EXPR$10), sex, asmt_grade, asmt_year, district_id as name  FROM s3.tmp.sampleparquet4 WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year,district_id, sex
union all
SELECT count(*), avg(EXPR$10), sex, asmt_grade, asmt_year, state_code as name  FROM s3.tmp.sampleparquet4 WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year, state_code , sex;
--38.849 seconds

-----------
create table s3.tmp.sampleparquetwithpartition(student_rec_id, sex, state_code, district_id, school_id, asmt_type, asmt_year, asmt_subject, asmt_grade,
   EXPR$10, asmt_perf_lvl) partition by (asmt_type, asmt_subject) as
 select student_rec_id, sex, state_code, district_id, school_id, asmt_type, asmt_year, asmt_subject, asmt_grade,
   EXPR$10, asmt_perf_lvl from s3.tmp.sampleparquet4;


SELECT count(*), avg(EXPR$10), sex, asmt_grade, asmt_year,  school_id as name FROM s3.tmp.sampleparquetwithpartition WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year,school_id, sex
union all
SELECT count(*), avg(EXPR$10), sex, asmt_grade, asmt_year, district_id as name  FROM s3.tmp.sampleparquetwithpartition WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year,district_id, sex
union all
SELECT count(*), avg(EXPR$10), sex, asmt_grade, asmt_year, state_code as name  FROM s3.tmp.sampleparquetwithpartition WHERE asmt_year IN (2015) AND asmt_grade IN ('4', '5') GROUP BY asmt_grade, asmt_year, state_code , sex;
--23.54 seconds)