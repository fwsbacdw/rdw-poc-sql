﻿SELECT
  count(*),
  avg(score),
  asmt_grade,
  asmt_year,
  school_id
FROM (
       SELECT
         student_rec_id,
         max(score) as score,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_id
       FROM s3.tmp.tinycap
       WHERE
         asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
         and
         (
           (asmt_year = 2015 and asmt_grade = '3') or
           (asmt_year = 2016 and asmt_grade = '4') or
           (asmt_year = 2017 and asmt_grade = '5')
         )
       GROUP BY
         student_rec_id ,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_id
     ) student_max_score
GROUP BY
  asmt_grade
  , asmt_year
  , school_id
ORDER BY school_id,
   asmt_year,
  asmt_grade;

-------
EXPLAIN plan for
SELECT
  count(*),
  avg(score),
  asmt_grade,
  asmt_year,
  school_id
FROM (
       SELECT
         student_rec_id,
         max(EXPR$01) as score,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_id
       FROM s3.tmp.sampleparquet4
       WHERE
         asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
         and
         (
           (asmt_year = 2015 and asmt_grade = '3') or
           (asmt_year = 2016 and asmt_grade = '4') or
           (asmt_year = 2017 and asmt_grade = '5')
         )
       GROUP BY
         student_rec_id ,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_id
     ) student_max_score
GROUP BY
  asmt_grade
  , asmt_year
  , school_id
ORDER BY school_id,
   asmt_year,
  asmt_grade;
-- 38.064 seconds

-----------

SELECT
  count(*),
  avg(score),
  asmt_grade,
  asmt_year,
  school_id
FROM (
       SELECT
         student_rec_id,
         max(EXPR$01) as score,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_id
       FROM s3.tmp.sampleparquetwithpartition
       WHERE
         asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
         and
         (
           (asmt_year = 2015 and asmt_grade = '3') or
           (asmt_year = 2016 and asmt_grade = '4') or
           (asmt_year = 2017 and asmt_grade = '5')
         )
       GROUP BY
         student_rec_id ,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         school_id
     ) student_max_score
GROUP BY
  asmt_grade
  , asmt_year
  , school_id
ORDER BY school_id,
   asmt_year,
  asmt_grade;
-- 18.258 seconds