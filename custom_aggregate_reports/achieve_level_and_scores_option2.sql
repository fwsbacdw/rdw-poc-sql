﻿-----------------------------------------------------------------------------------------------------------
-- Option 2: each row represents one type of a roll up (either state, district or school)
-- Each sub-group (such as gender and ethnicity) must be handled separately.
-- Below example demonstrates gender subgroup.

-- may need to add a parent id to know how to relate schools to districts
-----------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------
-- retrieve school data
-- TODO: consider removing gender dimension
-----------------------------------------------------------------------------------------------------------
SELECT
  count(*)          AS count,
  avg(score)        as score,
  sum(level1_value) AS level1,
  sum(level2_value) AS level2,
  sum(level3_value) AS level3,
  sum(level4_value) AS level4,
  ds.gender_id,
  da.asmt_grade,
  da.asmt_year,
  dh.school_name    AS name,
  'school'          AS type,
  'gender'          AS subgroup
FROM edware_ca.poc_fact_asmt fa
  JOIN edware_ca.poc_dim_school dh ON dh.id = fa.school_id
  JOIN edware_ca.poc_dim_student ds ON fa.student_id = ds.id
  JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
WHERE
  --dh.school_id IN ('f3fc491f648f4f85b9d9048b4f075b') AND
  da.asmt_year IN (2015) AND
  da.asmt_grade IN ('4', '5')
--AND da.asmt_type = 'INTERIM COMPREHENSIVE' --'SUMMATIVE'
--AND da.asmt_subject = 'Math' --'ELA'
GROUP BY da.asmt_grade
  , da.asmt_year
  , dh.school_name
  , dh.id
  , dh.district_id
  , ds.gender_id

UNION ALL

-----------------------------------------------------------------------------------------------------------
-- retrieve district data
-----------------------------------------------------------------------------------------------------------
SELECT
  count(*)          AS count,
  avg(score)        as score,
  sum(level1_value) AS level1,
  sum(level2_value) AS level2,
  sum(level3_value) AS level3,
  sum(level4_value) AS level4,
  ds.gender_id,
  da.asmt_grade,
  da.asmt_year,
  dh.district_name  AS name,
  'district'        AS type,
  'gender'          AS subgroup
FROM edware_ca.poc_fact_asmt fa
  JOIN edware_ca.poc_dim_district dh ON dh.id = fa.district_id
  JOIN edware_ca.poc_dim_student ds ON fa.student_id = ds.id
  JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
WHERE
  da.asmt_year IN (2015) AND
  da.asmt_grade IN ('4', '5')
--da.asmt_type = 'INTERIM COMPREHENSIVE' AND  --'SUMMATIVE'
--da.asmt_subject = 'Math' --'ELA'
GROUP BY da.asmt_grade
  , da.asmt_year
  , dh.id
  , ds.gender_id
  , dh.district_name
  , dh.state_code

UNION ALL

-----------------------------------------------------------------------------------------------------------
-- retrieve state data
-----------------------------------------------------------------------------------------------------------
SELECT
  count(*)          AS count,
  avg(score)        as score,
  sum(level1_value) AS level1,
  sum(level2_value) AS level2,
  sum(level3_value) AS level3,
  sum(level4_value) AS level4,
  ds.gender_id,
  da.asmt_grade,
  da.asmt_year,
  dh.state_code     AS name,
  'state'           AS type,
  'gender'          AS subgroup
FROM edware_ca.poc_fact_asmt fa
  JOIN edware_ca.poc_dim_state dh ON dh.state_code = fa.state_code
  JOIN edware_ca.poc_dim_student ds ON fa.student_id = ds.id
  JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
WHERE
  dh.state_code IN ('CA') AND
  da.asmt_year IN (2015) AND
  da.asmt_grade IN ('4', '5')
--da.asmt_type = 'INTERIM COMPREHENSIVE' AND --'SUMMATIVE'
--da.asmt_subject = 'Math' --'ELA'
GROUP BY ds.gender_id
  , da.asmt_grade
  , da.asmt_year
  , dh.state_code
	