﻿-----------------------------------------------------------------------------------------------------------
-- Option 1: each row represents a school, and corresponding district and state roll ups
-- Each sub-group (such as gender and ethnicity) must be handled separately.
-- Below example demonstrates gender subgroup.

-- If schools and districts without schools are mixed - then two queries must be executed and joinded by UNION
-- Ex.: State
--        District A
--          School A
--       District B
--       District C
-- With the above use case we cannot run one query. It has to be broken into two: one for SchoolA/DistrictA and another for
-- other districts/schools

-- Cons:
--      Assumes that a higher level roll up is always needed.
--      Two unions, bust state value is calculated twice
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-- the below query is for the use case when school(s) are requested (individual or all schools)
-- one query per each sub-group is required
-- it will return the requested schools, year, grades, sub-group breakdown as well as the roll up to district
-- and state the given schools belong to
-----------------------------------------------------------------------------------------------------------
SELECT
  s.school_count,
  s.school_score,
  s_level1,
  s_level2,
  s_level3,
  s_level4,
  dg.value,
  s.asmt_grade,
  s.asmt_year,
  s.school_name,
  d.district_count,
  d.district_score,
  d_level1,
  d_level2,
  d_level3,
  d_level4,
  d.district_name,
  st.state_count,
  st.state_score,
  st_level1,
  st_level2,
  st_level3,
  st_level4,
  st.state_code
FROM (
-----------------------------------------------------------------------------------------------------------
-- retrieve school data
-----------------------------------------------------------------------------------------------------------
       SELECT
         count(*)          AS school_count,
         avg(score)        AS school_score,
         sum(level1_value) AS s_level1,
         sum(level2_value) AS s_level2,
         sum(level3_value) AS s_level3,
         sum(level4_value) AS s_level4,
         fa.gender_id,
         da.asmt_grade,
         da.asmt_year,
         dh.school_name,
         dh.district_id
       FROM edware_ca.poc_fact_asmt fa
         JOIN edware_ca.poc_dim_school dh ON dh.id = fa.school_id
         JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
       WHERE
         --dh.school_id IN ('f3fc491f648f4f85b9d9048b4f075b') AND
         da.asmt_year IN (2015) AND
         da.asmt_grade IN ('4', '5')
       --AND da.asmt_type = 'INTERIM COMPREHENSIVE' --'SUMMATIVE'
       --AND da.asmt_subject = 'Math' --'ELA'
       GROUP BY da.asmt_grade
         , da.asmt_year
         , dh.school_name
         , dh.id
         , dh.district_id
         , fa.gender_id
     ) s
  INNER JOIN (
-----------------------------------------------------------------------------------------------------------
-- retrieve district data for each school from the above response and append to schools
-----------------------------------------------------------------------------------------------------------
               SELECT
                 count(*)          AS district_count,
                 avg(score)        AS district_score,
                 sum(level1_value) AS d_level1,
                 sum(level2_value) AS d_level2,
                 sum(level3_value) AS d_level3,
                 sum(level4_value) AS d_level4,
                 fa.gender_id,
                 da.asmt_grade,
                 da.asmt_year,
                 dh.id,
                 dh.district_name,
                 dh.state_code
               FROM edware_ca.poc_fact_asmt fa
                 JOIN edware_ca.poc_dim_district dh ON dh.id = fa.district_id
                 JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
               WHERE
                 da.asmt_year IN (2015) AND
                 da.asmt_grade IN ('4', '5')
               --da.asmt_type = 'INTERIM COMPREHENSIVE' AND  --'SUMMATIVE'
               --da.asmt_subject = 'Math' --'ELA'
               GROUP BY da.asmt_grade
                 , da.asmt_year
                 , dh.id
                 , fa.gender_id
                 , dh.district_name
                 , dh.state_code
             ) d ON d.id = s.district_id
                    AND d.gender_id = s.gender_id
                    AND d.asmt_grade = s.asmt_grade
                    AND d.asmt_year = s.asmt_year
  INNER JOIN (
  -----------------------------------------------------------------------------------------------------------
-- retrieve state data for a district from the above response and append to the school record
-----------------------------------------------------------------------------------------------------------
               SELECT
                 count(*)          AS state_count,
                 avg(score)        AS state_score,
                 sum(level1_value) AS st_level1,
                 sum(level2_value) AS st_level2,
                 sum(level3_value) AS st_level3,
                 sum(level4_value) AS st_level4,
                 fa.gender_id,
                 da.asmt_grade,
                 da.asmt_year,
                 dh.state_code
               FROM edware_ca.poc_fact_asmt fa
                 JOIN edware_ca.poc_dim_state dh ON dh.state_code = fa.state_code
                 JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
               WHERE
                 dh.state_code IN ('CA') AND
                 da.asmt_year IN (2015) AND
                 da.asmt_grade IN ('4', '5')
               --da.asmt_type = 'INTERIM COMPREHENSIVE' AND --'SUMMATIVE'
               --da.asmt_subject = 'Math' --'ELA'
               GROUP BY fa.gender_id
                 , da.asmt_grade
                 , da.asmt_year
                 , dh.state_code
             ) st ON d.state_code = st.state_code
                     AND d.gender_id = st.gender_id
                     AND d.asmt_grade = st.asmt_grade
                     AND d.asmt_year = st.asmt_year
  INNER JOIN edware_ca.poc_dim_gender dg ON dg.id = s.gender_id

UNION ALL
-----------------------------------------------------------------------------------------------------------
-- the below query is for the use case when districts WITHOUT schools are requested (individual or all)
-- it will return the requested district, year, grades, sub-group breakdown as well as the roll up to state the given district belong to
----------------------------------------------------------------------------------------------------------
SELECT
  NULL AS school_count,
  NULL AS school_score,
  NULL AS s_level1,
  NULL AS s_level2,
  NULL AS s_level3,
  NULL AS s_level4,
  dg.value,
  d.asmt_grade,
  d.asmt_year,
  NULL AS school_name,
  d.district_count,
  d.district_score,
  d_level1,
  d_level2,
  d_level3,
  d_level4,
  d.district_name,
  st.state_count,
  st.state_score,
  st_level1,
  st_level2,
  st_level3,
  st_level4,
  st.state_code
FROM (
       SELECT
         count(*)          AS district_count,
         avg(score)        AS district_score,
         sum(level1_value) AS d_level1,
         sum(level2_value) AS d_level2,
         sum(level3_value) AS d_level3,
         sum(level4_value) AS d_level4,
         fa.gender_id,
         da.asmt_grade,
         da.asmt_year,
         dh.district_id,
         dh.district_name,
         dh.state_code
       FROM edware_ca.poc_fact_asmt fa
         JOIN edware_ca.poc_dim_school dh ON dh.id = fa.school_id
         JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
       WHERE
         da.asmt_year IN (2015) AND
         da.asmt_grade IN ('4', '5')
       --da.asmt_type = 'INTERIM COMPREHENSIVE' AND  --'SUMMATIVE'
       --da.asmt_subject = 'Math' --'ELA'
       GROUP BY da.asmt_grade
         , da.asmt_year
         , dh.district_id
         , fa.gender_id
         , dh.district_name
         , dh.state_code
     ) d
  INNER JOIN (
               SELECT
                 count(*)          AS state_count,
                 avg(score)        AS state_score,
                 sum(level1_value) AS st_level1,
                 sum(level2_value) AS st_level2,
                 sum(level3_value) AS st_level3,
                 sum(level4_value) AS st_level4,
                 fa.gender_id,
                 da.asmt_grade,
                 da.asmt_year,
                 dh.state_code
               FROM edware_ca.poc_fact_asmt fa
                 JOIN edware_ca.poc_dim_district dh ON dh.id = fa.district_id
                 JOIN edware_ca.poc_dim_asmt da ON da.id = fa.asmt_id
               WHERE
                 dh.state_code IN ('CA') AND
                 da.asmt_year IN (2015) AND
                 da.asmt_grade IN ('4', '5')
               --da.asmt_type = 'INTERIM COMPREHENSIVE' AND --'SUMMATIVE'
               --da.asmt_subject = 'Math' --'ELA'
               GROUP BY fa.gender_id
                 , da.asmt_grade
                 , da.asmt_year
                 , dh.state_code
             ) st ON d.state_code = st.state_code
                     AND d.gender_id = st.gender_id
                     AND d.asmt_grade = st.asmt_grade
                     AND d.asmt_year = st.asmt_year
  INNER JOIN edware_ca.poc_dim_gender dg ON dg.id = d.gender_id
  ORDER BY district_name, school_name, asmt_grade