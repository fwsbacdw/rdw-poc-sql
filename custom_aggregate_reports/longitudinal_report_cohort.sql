﻿------------------------------------------------------------------------------------------------------------
-- group of students in cohort throughout all selected years
------------------------------------------------------------------------------------------------------------
SELECT
  count(*),
  cast(avg(score) AS INT),
  asmt_grade,
  asmt_year,
  school_id
FROM (
       -- get the highest student score within a school
       SELECT
         fa.student_id,
         max(score) as score,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         fa.school_id
       FROM
         edware_ca.poc_dim_asmt da
         JOIN edware_ca.poc_fact_asmt fa ON fa.asmt_id = da.id
         JOIN (
                -- count how many times a student appears in the same school for the selected grades/assessments
                SELECT
                  count(*) as count,
                  student_id,
                  school_id
                FROM
                  (
                    SELECT distinct
                      student_id,
                      asmt_grade,
                      asmt_year,
                      asmt_type,
                      asmt_subject,
                      school_id
                    FROM
                      edware_ca.poc_dim_asmt da
                      JOIN edware_ca.poc_fact_asmt fa ON fa.asmt_id = da.id
                    WHERE
                      asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
                      AND
                      (
                        (asmt_year = 2015 AND asmt_grade = '3') OR
                        (asmt_year = 2016 AND asmt_grade = '4') OR
                        (asmt_year = 2017 AND asmt_grade = '5')
                      )
                    GROUP BY student_id, asmt_grade, asmt_year, asmt_type, asmt_subject, school_id
                  ) distinct_student
                GROUP BY student_id, school_id
              ) AS year_in_one_hier
           ON year_in_one_hier.student_id = fa.student_id and year_in_one_hier.school_id = fa.school_id
       WHERE
         asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
         AND
         (
           (asmt_year = 2015 AND asmt_grade = '3') OR
           (asmt_year = 2016 AND asmt_grade = '4') OR
           (asmt_year = 2017 AND asmt_grade = '5')
         )
         and year_in_one_hier.count = 3
       GROUP BY fa.student_id, asmt_grade, asmt_year, asmt_type, asmt_subject, fa.school_id
     ) student_cohort
GROUP BY
  asmt_grade
  , asmt_year
  , school_id
ORDER BY school_id
  , asmt_year
  , asmt_grade