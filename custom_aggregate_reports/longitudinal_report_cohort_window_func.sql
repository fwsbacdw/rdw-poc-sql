﻿------------------------------------------------------------------------------------------------------------
-- group of students in cohort throughout all selected years
------------------------------------------------------------------------------------------------------------
SELECT
  count(*),
  cast(avg(score) AS INT),
  asmt_grade,
  asmt_year,
  school_id
FROM (
  -- get the highest student score within a school
  SELECT
    student_id,
    max(score) AS score,
    asmt_grade,
    asmt_year,
    asmt_type,
    asmt_subject,
    school_id,
    count(*) OVER (PARTITION BY student_id, school_id) AS year_in_one_inst
  FROM
    edware_ca.poc_dim_asmt da
    JOIN edware_ca.poc_fact_asmt fa ON fa.asmt_id = da.id
  WHERE
    asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
    AND
    (
      (asmt_year = 2015 AND asmt_grade = '3') OR
      (asmt_year = 2016 AND asmt_grade = '4') OR
      (asmt_year = 2017 AND asmt_grade = '5')
    )
  GROUP BY student_id, asmt_grade, asmt_year, asmt_type, asmt_subject, school_id
) s
-- this needs to match the number of years included, it removes students that are not found in all the years
WHERE year_in_one_inst = 3
GROUP BY
asmt_grade
, asmt_year
, school_id
ORDER BY school_id
, asmt_year
, asmt_grade