﻿----------------------------------------------------------------------------------------------------------
-- 2015
--    District A
--      School A1
--        Grade 3
--          Student 100 - asmt 315, ELA, ICA
--                        asmt 3152, ELA, ICA
--                        asmt 315, ELA, ICA
--          Student 102 - asmt 315, ELA, ICA
--                        asmt 3152, ELA, ICA
--                        asmt 315, ELA, ICA
--          Student 103 - asmt 315, ELA, ICA
--          Student 104 - asmt 315, ELA, ICA
--          Student 105 - asmt 315, ELA, ICA
--       Grade 4
--          Student 150 - asmt 415
--      School A2
--                  -------------------------
--    District B
--      School B1
--        Grade 3
--          Student 200 - asmt 315, ELA, ICA
--                        asmt 3152, ELA, ICA
--                        asmt 315, ELA, ICA

-- 2016
--    District A
--      School A1
--        Grade 4
--          Student 100 - asmt 416
--          Student 103 - asmt 416
--          Student 104 - asmt 416, ELA, ICA
--          Student 105 - asmt 416, ELA, ICA
--       Grade 5
--          Student 150 - asmt 516
--      School A2
--        Grade 4
--          Student 102 - asmt 416
--    District B
--      School B1
--        Grade 4
--          Student 200 - asmt 416

-- 2017
--    District A
--      School A1
--        Grade 5
--          Student 100 - asmt 516
--          Student 102 - asmt 516
--          Student 104 - asmt 516, ELA, ICA
--          Student 105 - asmt 516, ELA, ICA
--       Grade 6
--          Student 150 - asmt 617
--      School A2
--        Grade 5
--          Student 105 - asmt 516, ELA, ICA  - large score
--    District B
--      School B1
--        Grade 5
--          Student 103 - asmt 516
--          Student 104 - asmt 315, ELA, ICA - large score
--          Student 200 - asmt 516

----------------------------------------------------------------------------------------------------------

with poc_dim_district as(
  select 'district a' as district_name, 1 as id
  union all
  select 'district b' as district_name, 2 as id
),

    poc_dim_school as(
    select 10 as id, 'school a1' as school_name, 'district a' as district_name, 1 as district_id
    union all
    select 11 as id, 'school a2' as school_name, 'district a' as district_name, 1 as district_id
    union all
    select 20 as id, 'school b1' as school_name, 'district b' as district_name, 2 as district_id
  ),

    poc_dim_asmt as (
    select 315 as id, 3 as asmt_grade, 2015 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 3152 as id, 3 as asmt_grade, 2015 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 415 as id, 4 as asmt_grade, 2015 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 515 as id, 5 as asmt_grade, 2015 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 316 as id, 3 as asmt_grade, 2016 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 416 as id, 4 as asmt_grade, 2016 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 516 as id, 5 as asmt_grade, 2016 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 317 as id, 3 as asmt_grade, 2017 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 417 as id, 4 as asmt_grade, 2017 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
    union all
    select 517 as id, 5 as asmt_grade, 2017 as asmt_year, 'ICA' as asmt_type, 'ELA' as asmt_subject
  ),

    poc_fact_asmt as (

    -- this student needs to be filteres when searching grade 3, 2015 because (s)he is in grade 4
    select 150 as student_id, 50000000 as score, 10 as school_Id, 1 as district_id, 415 as asmt_id
    union all
    select 150 as student_id, 50000000 as score, 10 as school_Id, 1 as district_id, 415 as asmt_id -- the same assmt twice
    union all
    select 150 as student_id, 50000000 as score, 10 as school_Id, 1 as district_id, 516 as asmt_id
    union all
    select 150 as student_id, 50000000 as score, 10 as school_Id, 1 as district_id, 617 as asmt_id

    union all

    -- this student stay all three years with the same school (10) and took three assessments in 2015
    select 100 as student_id, 100 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id
    union all
    select 100 as student_id, 130 as score, 10 as school_Id, 1 as district_id, 3152 as asmt_id -- multiple assmts in the same year
    union all
    select 100 as student_id, 150 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id -- the same assmt twice
    union all
    select 100 as student_id, 200 as score, 10 as school_Id, 1 as district_id, 416 as asmt_id
    union all
    select 100 as student_id, 300 as score, 10 as school_Id, 1 as district_id, 517 as asmt_id

    union all

    -- this student left school in 2016 (change school to the same district) and came back in the 2017,
    -- also took multiple assessments in 2015
    select 102 as student_id, 100 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id
    union all
    select 102 as student_id, 130 as score, 10 as school_Id, 1 as district_id, 3152 as asmt_id -- multiple assmts in the same year
    union all
    select 102 as student_id, 150 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id -- the same assmt twice
    union all
    select 102 as student_id, 500 as score, 11 as school_Id, 1 as district_id, 416 as asmt_id
    union all
    select 102 as student_id, 300 as score, 10 as school_Id, 1 as district_id, 517 as asmt_id

    union all

    -- this student left school in 2017 (change school to the diff district)
    select 103 as student_id, 100 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id
    union all
    select 103 as student_id, 300 as score, 10 as school_Id, 1 as district_id, 416 as asmt_id
    union all
    select 103 as student_id, 700 as score, 20 as school_Id, 2 as district_id, 517 as asmt_id

    -- this student left school in 2017 (change school to the diff district), but half through the year,
    -- and was able to take assessments in both schools/districts
    union all
    select 104 as student_id, 100 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id
    union all
    select 104 as student_id, 300 as score, 10 as school_Id, 1 as district_id, 416 as asmt_id
    union all
    select 104 as student_id, 7000 as score, 20 as school_Id, 2 as district_id, 517 as asmt_id
    union all
    select 104 as student_id, 700 as score, 10 as school_Id, 1 as district_id, 517 as asmt_id

    union all

    -- this student left school in 2017 (change school but not district), but half through the year,
    -- and was able to take assessments in both schools
    select 105 as student_id, 100 as score, 10 as school_Id, 1 as district_id, 315 as asmt_id
    union all
    select 105 as student_id, 300 as score, 10 as school_Id, 1 as district_id, 416 as asmt_id
    union all
    select 105 as student_id, 700 as score, 10 as school_Id, 1 as district_id, 517 as asmt_id
    union all
    select 105 as student_id, 1000 as score, 11 as school_Id, 1 as district_id, 517 as asmt_id

    union all

    --student 3 school b1
    select 200 as student_id, 100 as score, 20 as school_Id, 2 as district_id, 315 as asmt_id
    union all
    select 200 as student_id, 130 as score, 20 as school_Id, 2 as district_id, 3152 as asmt_id -- multiple assmts in the same year
    union all
    select 200 as student_id, 150 as score, 20 as school_Id, 2 as district_id, 315 as asmt_id -- the same assmt twice
    union all
    select 200 as student_id, 200 as score, 20 as school_Id, 2 as district_id, 416 as asmt_id
    union all
    select 200 as student_id, 300 as score, 20 as school_Id, 2 as district_id, 517 as asmt_id
  )
------------------------------------------------------------------------------------------------------------
-- count of students tested (without cohort) with the max score per assessment year/grade/subject
------------------------------------------------------------------------------------------------------------
SELECT
  count(*),
  cast(avg(score) AS INT),
  asmt_grade,
  asmt_year,
  school_id
FROM (
       -- get the highest student score within a school
       SELECT
         fa.student_id,
         max(score) as score,
         asmt_grade,
         asmt_year,
         asmt_type,
         asmt_subject,
         fa.school_id
       FROM
         poc_dim_asmt da
         JOIN poc_fact_asmt fa ON fa.asmt_id = da.id
         JOIN (
                -- count how many times a student appears in the same school for the selected grades/assessments
                SELECT
                  count(*) as count,
                  student_id,
                  school_id
                FROM
                  (
                    SELECT distinct
                      student_id,
                      asmt_grade,
                      asmt_year,
                      asmt_type,
                      asmt_subject,
                      school_id
                    FROM
                      poc_dim_asmt da
                      JOIN poc_fact_asmt fa ON fa.asmt_id = da.id
                    WHERE
                      asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
                      AND
                      (
                        (asmt_year = 2015 AND asmt_grade = '3') OR
                        (asmt_year = 2016 AND asmt_grade = '4') OR
                        (asmt_year = 2017 AND asmt_grade = '5')
                      )
                    GROUP BY student_id, asmt_grade, asmt_year, asmt_type, asmt_subject, school_id
                  ) distinct_student
                GROUP BY student_id, school_id
              ) AS year_in_one_hier
           ON year_in_one_hier.student_id = fa.student_id and year_in_one_hier.school_id = fa.school_id
       WHERE
         asmt_subject = 'ELA' AND asmt_type <> 'SUMMATIVE'
         AND
         (
           (asmt_year = 2015 AND asmt_grade = '3') OR
           (asmt_year = 2016 AND asmt_grade = '4') OR
           (asmt_year = 2017 AND asmt_grade = '5')
         )
         and year_in_one_hier.count = 3
       GROUP BY fa.student_id, asmt_grade, asmt_year, asmt_type, asmt_subject, fa.school_id
     ) student_cohort
GROUP BY
  asmt_grade
  , asmt_year
  , school_id
ORDER BY school_id
  , asmt_year
  , asmt_grade
